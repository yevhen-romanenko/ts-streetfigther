"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function createElement(tagName, className, attributes = {}) {
    const element = document.createElement(tagName);
    if (className) {
        const classNames = className.split('').filter(Boolean);
        element.classList.add(...classNames);
        Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
        return element;
    }
}
exports.createElement = createElement;
// module.exports createElement;
//# sourceMappingURL=domHelper.js.map