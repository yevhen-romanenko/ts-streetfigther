import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise(
    (resolve) => {
      // resolve the promise with the winner when fight is over

      // healthbars and fighters into constants

      const healthBarsList = document.querySelectorAll('.arena___health-bar');
      const healthBars = [...healthBarsList];
      const combatViewList = document.querySelectorAll('.arena___health-indicator');
      const combatView = [...combatViewList];
      const hitpoints = [firstFighter.health, secondFighter.health];

      const combatRoundInfo = {
        playerBlock: false,
        currentHealth: hitpoints,
        critTime: null,
        critInputCombo: [],
      };

      const fighterOne = {
        ...firstFighter,
        ...combatRoundInfo,
        maxHpHero: hitpoints[0],
        currentHealth: hitpoints[0],
        healthBar: healthBars[0],
        combatView: combatView[0],
        position: 'left',
      };

      const fighterTwo = {
        ...secondFighter,
        ...combatRoundInfo,
        maxHpHero: hitpoints[1],
        currentHealth: hitpoints[1],
        healthBar: healthBars[1],
        combatView: combatView[1],
        position: 'right',
      };

      function showCombatText(fighter, text) {
        if (document.getElementById(`${fighter.position}-combat-marker`)) {
          document.getElementById(`${fighter.position}-combat-marker`).remove();
        }

        const combatText = createElement({
          tagName: 'div',
          className: 'arena___combat-marker',
          attributes: { id: `${fighter.position}-combat-marker` },
        });

        combatText.innerText = text;
        combatText.style.opacity = 1;

        fighter.combatView.append(combatText);

        setInterval(() => {
          if (combatText.style.opacity > 0) {
            combatText.style.opacity -= 0.01;
          } else {
            combatText.remove();
          }
        }, 10);
      }

      function roundOfFigth(attacker, defender) {
        if (attacker.block) {
          showCombatText(attacker, 'U cant hit! Stop blocking yourself!');
          return void 0;
        }

        if (defender.block) {
          showCombatText(defender, 'Block is up!');
          return void 0;
        }

        if (attacker.critInputCombo.length === 3) {
          // defender.block = false;
          const critDamage = criticalHit(attacker);
          defender.currentHealth -= critDamage;

          showCombatText(attacker, 'Critical hit!');
          showCombatText(defender, `-${critDamage}`);
        } else {
          const totalDamage = getDamage(attacker, defender);

          if (totalDamage <= 0) {
            showCombatText(attacker, 'Enemy Dodged!');
            return void 0;
          }

          showCombatText(defender, `-${totalDamage.toFixed(2)}`);

          defender.currentHealth = defender.currentHealth - totalDamage;
        }

        if (defender.currentHealth <= 0) {
          defender.healthBar.style.width = `0%`;
          document.removeEventListener('keydown', keyDown);
          document.removeEventListener('keyup', keyUp);
          resolve(attacker);
        }

        defender.healthBar.style.width = `${(defender.currentHealth / defender.maxHpHero) * 100}%`;
        
      }

      // change callback event from anonymos to functions with real names

      function keyUp(event) {
        if (event.code === controls.PlayerOneBlock) {
          fighterOne.block = false;
        }
        if (event.code === controls.PlayerTwoBlock) {
          fighterTwo.block = false;
        }

        if (fighterOne.critInputCombo.includes(event.code)) {
          fighterOne.critInputCombo.splice(fighterOne.critInputCombo.indexOf(event.code), 1);
        }

        if (fighterTwo.critInputCombo.includes(event.code)) {
          fighterTwo.critInputCombo.splice(fighterTwo.critInputCombo.indexOf(event.code), 1);
        }
      }

      function keyDown(event) {
        if (!event.repeat) {
          switch (event.code) {
            case controls.PlayerOneAttack: {
              roundOfFigth(fighterOne, fighterTwo);
              break;
            }

            case controls.PlayerTwoAttack: {
              roundOfFigth(fighterTwo, fighterOne);
              break;
            }

            case controls.PlayerOneBlock: {
              fighterOne.block = true;
              break;
            }

            case controls.PlayerTwoBlock: {
              fighterTwo.block = true;
              break;
            }
          }

          if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
            if (getCriticalHandler(fighterOne)) {
              fighterTwo.block = false;
              roundOfFigth(fighterOne, fighterTwo);
            } else return null;
          }

          if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
            if (getCriticalHandler(fighterTwo)) {
              fighterOne.block = false;
              roundOfFigth(fighterTwo, fighterOne);
            } else return null;
          }
        }
      }

      document.addEventListener('keydown', keyDown);
      document.addEventListener('keyup', keyUp);
    }
    // firstFighter,
    // secondFighter
  );
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const powerHit = fighter.attack * criticalHitChance;
  return powerHit;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const powerDef = fighter.defense * dodgeChance;
  return powerDef;
}

export function getCriticalHandler(fighter) {
  // return critical hit
  const currentTime = Date.now();

  if (currentTime - fighter.critTime < 10000) {
    return false;
  }

  if (!fighter.critInputCombo.includes(event.code)) {
    fighter.critInputCombo.push(event.code);
  }

  if (fighter.critInputCombo.length === 3) {
    fighter.critTime = currentTime;
    return true;
  }
}

export function criticalHit(fighter) {
  const criticalHit = fighter.attack * 2;

  return criticalHit;
}
