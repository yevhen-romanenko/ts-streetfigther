import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';


export function showWinnerModal(fighter) {
  // call showModal function
  // console.log('Winner from modal ->', fighter);

  const title = `Winner is : ${fighter.name} `;
  
  const attributes = {
    src: fighter.source,
    alt: fighter.name,
  };
  const fighterImage = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  const bodyElement = fighterImage;

  showModal({ title, bodyElement });
}
