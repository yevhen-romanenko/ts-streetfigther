import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    fighterElement.style.color = 'black';
    fighterElement.style.fontFamily = 'Charcoal,monospace';
    fighterElement.innerHTML = `<div>
      <h2>Fighter: ${fighter.name}</h2>
      <p>HP: <b>${fighter.health}</b></p>
      <p>ATK: <b>${fighter.attack}</b></p>
      <p>DEF: <b>${fighter.defense}</b></p>
    </div>`;

    const attributes = {
      src: fighter.source,
      alt: fighter.name,
    };
    const fighterImage = createElement({
      tagName: 'img',
      className: 'fighter-preview___img',
      attributes,
    });

    fighterElement.appendChild(fighterImage);
  }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
