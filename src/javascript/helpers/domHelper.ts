export function createElement(tagName: string, className: string, attributes: object = {}): HTMLElement | void {
  const element = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split('').filter(Boolean);
    element.classList.add(...classNames);

    Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

    return element;
  }
}

// module.exports createElement;
