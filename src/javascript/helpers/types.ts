export interface fighter {
  _id: string;
  name: string;
  source: string;
}

export interface fightersDetail {
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
}
